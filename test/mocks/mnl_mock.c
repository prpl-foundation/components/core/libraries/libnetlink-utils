/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <cmocka.h>
#include <string.h>
#include <netdb.h>

#include <libmnl/libmnl.h>

#include "mock.h"

void __wrap_mnl_attr_put_u8(struct nlmsghdr* nlh, uint16_t type, uint8_t data) {
    assert_non_null(nlh);
}

void __wrap_mnl_attr_put_u16(struct nlmsghdr* nlh, uint16_t type, uint16_t data) {
    assert_non_null(nlh);
}

void __wrap_mnl_attr_put_u32(struct nlmsghdr* nlh, uint16_t type, uint32_t data) {
    assert_non_null(nlh);
}

void mnl_attr_put_u64(struct nlmsghdr* nlh, uint16_t type, uint64_t data) {
    assert_non_null(nlh);
}

ssize_t __wrap_mnl_socket_recvfrom(const struct mnl_socket* nl, void* buf, size_t siz) {
    assert_non_null(nl);
    return 10;
}

ssize_t __wrap_mnl_socket_sendto(const struct mnl_socket* nl, const void* req, size_t siz) {
    assert_non_null(nl);
    return 10;
}

extern int __wrap_mnl_socket_setsockopt(const struct mnl_socket* nl, int type, void* buf, socklen_t len) {
    assert_non_null(nl);
    assert_non_null(buf);
    return 0;
}

int __wrap_mnl_socket_bind(struct mnl_socket* nl, UNUSED unsigned int groups, UNUSED pid_t pid) {
    assert_non_null(nl);
    return 0;
}

int __wrap_mnl_socket_get_fd(const struct mnl_socket* nl) {
    assert_non_null(nl);
    return 10;
}

struct mnl_socket* __wrap_mnl_socket_open(int type) {
    struct mnl_socket* nl = calloc(1, sizeof(struct mnl_socket));
    assert_non_null(nl);
    return nl;
}

int __wrap_mnl_socket_close(struct mnl_socket* nl) {
    assert_non_null(nl);
    free(nl);
    return 0;
}
