/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NFQUEUE_H__
#define __NFQUEUE_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <netinet/in.h>
#include <linux/netfilter.h>

#include <netlink-utils/packet.h>

typedef struct nfqueue_t nfqueue_t;

typedef struct {
    uint16_t group;         // NFQUEUE group number of the packet.
    uint32_t id;            // NFQUEUE ID of the packet.
} nfqueue_data_t;

/**
 * Creates a new nfqueue socket.
 * @param nfqueue           Pointer to a pointer for a new nfqueue_t struct.
 * @param qnum              Queue number that should be listened to.
 * @param ip_version	    IP version of packets that should be listened to. 0 means all.
 * @param conntrack_info    Bool indicating if conntrack info should be added to the output.
 *
 * @return 0 if success, -1 if an error occured
 */
int nfqueue_new(nfqueue_t** nfqueue, uint16_t qnum, int8_t ip_version, bool conntrack_info);

/**
 * Deletes a nfqueue struct.
 * @param nfqueue       Pointer to a pointer to a valid nfqueue_t struct.
 * @param close_socket  Bool indicating if the underlying socket should be closed.
 */
void nfqueue_delete(nfqueue_t** nfqueue, bool close_socket);

/**
 * Returns the file descriptor of the nfqueue socket.
 * @param nfqueue       Nfqueue_t struct to receive a fd from.
 *
 * @return The fd if succesful, -1 otherwise.
 */
int nfqueue_get_fd(nfqueue_t* nfqueue);

/**
 * Returns the queue number associated with the nfqueue_t struct.
 * @param nfqueue       Nfqueue_t struct get the queue number from.
 *
 * @return The queue number if succesful, -1 otherwise.
 */
uint16_t nfqueue_get_qnum(nfqueue_t* nfqueue);

/**
 * Tries to create a nfqueue struct from a file descriptor.
 * @param nfqueue   Pointer to a pointer to a  valid nfqueue_t struct.
 * @param fd        The netlink file descriptor
 * @param qnum      The queue number of the associated queue.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int nfqueue_get(nfqueue_t** nfqueue, int fd, int qnum);

/**
 * Reads a NFQUEUE packet from the socket.
 * The callback will be called if the packet was read succesfully.
 * @param nfqueue   Nfqueue_t struct to read data from.
 * @param nfqdata   Pointer to a nfqueue_data_t struct that will be filled in by this call.
 * @param packet    Pointer to a pointer to a packet_data_t struct that will be filled in by this call.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int nfqueue_read(nfqueue_t* nfqueue, nfqueue_data_t* nfqdata, packet_data_t** packet);

/**
 * Give a verdict to a packet.
 * @param nfqueue       Nfqueue_t struct the packet was received from.
 * @param packetid	    ID assigned to the packet to identify it.
 * @param pkt_verdict   Verdict to return to netfilter (NF_ACCEPT, NF_DROP, ...).
 *
 * @return 0 if succesful, -1 otherwise.
 */
int nfqueue_judge_packet(nfqueue_t* nfqueue, uint32_t pkt_id, uint32_t pkt_verdict);

/**
 * Give a verdict to a packet and set the skbmark and connmark.
 * @param nfqueue       Nfqueue_t struct the packet was received from.
 * @param packetid	    ID assigned to the packet to identify it.
 * @param pkt_verdict   Verdict to return to netfilter (NF_ACCEPT, NF_DROP, ...).
 * @param pkt_mark      Mark to put on packet.
 * @param pkt_connmark  Connmark to put on packet.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int nfqueue_judge_packet_mark(nfqueue_t* nfqueue, uint32_t pkt_id, uint32_t pkt_verdict, uint32_t pkt_mark, uint32_t pkt_connmark);

#endif /* __NFQUEUE_H__ */
