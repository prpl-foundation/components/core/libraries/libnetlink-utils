/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NFLOG_H__
#define __NFLOG_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <netinet/in.h>
#include <linux/netfilter.h>

#include <netlink-utils/packet.h>

typedef struct nflog_t nflog_t;

typedef struct {
    uint16_t group;
    const char* prefix;
} nflog_data_t;

/**
 * Create a new nflog socket.
 * @param nflog             Pointer to a pointer for a new nflog_t struct.
 * @param gnum              Group number that should be listened to.
 * @param ip_version	    IP version of packets that should be listened to. 0 means all.
 * @param conntrack_info    Bool indicating if conntrack info should be added to the output.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int nflog_new(nflog_t** nflog, uint16_t gnum, int8_t ip_version, bool conntrack_info);

/**
 * Delete a nflog_t struct.
 * @param nflog         Pointer to a pointer to a valid nflog_t struct.
 * @param close_socket  Bool indicating if the underlying socket should be closed.
 */
void nflog_delete(nflog_t** nflog, bool close_socket);

/**
 * Returns the file descriptor of the nflog_t socket.
 * @param nflog         Nflog_t struct to recv a fd from.
 *
 * @return The fd if succesful, -1 otherwise.
 */
int nflog_get_fd(nflog_t* nflog);

/**
 * Returns the group number associated with the nflog_t struct.
 * @param nflog       Nflog_t struct to get the group number from.
 *
 * @return The group number if succesful, -1 otherwise.
 */
uint16_t nflog_get_gnum(nflog_t* nflog);

/**
 * Tries to create a nflog_t struct from a file descriptor.
 * @param nflog     Pointer to a pointer to a valid nflog_t struct.
 * @param fd        The netlink file descriptor
 * @param gnum      The group number of the associated nflog.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int nflog_get(nflog_t** nflog, int fd, int qnum);

/**
 * Reads a NFLOG packet from the socket.
 * The callback will be called if the packet was read succesfully.
 * @param nflog     Nflog struct to read data from.
 * @param nfqdata   Pointer to a nflog_data_t struct that will be filled in by this call.
 * @param packet    Pointer to a pointer to a packet_data_t struct that will be filled in by this call.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int nflog_read(nflog_t* nflog, nflog_data_t* nfldata, packet_data_t** packet);

#endif /* __NFLOG_H__ */