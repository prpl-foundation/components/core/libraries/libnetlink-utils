/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __CONNTRACK_H__
#define __CONNTRACK_H__

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>

#include <linux/netfilter/nfnetlink_conntrack.h>
#include <linux/netfilter/nf_conntrack_tcp.h>

typedef struct conntrack_t conntrack_t;

typedef struct {
    int in_addr_type;
    union addr_union {
        struct in_addr in4_addr;
        struct in6_addr in6_addr;
    } addr;
} ip_addr_t;

typedef struct {
    uint32_t id;
    uint32_t mark;
    uint64_t packets_orig;
    uint64_t packets_repl;
    uint64_t bytes_orig;
    uint64_t bytes_repl;
    uint8_t l3protocol;
    ip_addr_t src;
    ip_addr_t dst;
    uint8_t l4protocol;
    uint16_t src_port;
    uint16_t dst_port;
    enum tcp_conntrack tcp_state;
} conntrack_data_t;

typedef enum {
    CONNTRACK_NONE = 0,
    CONNTRACK_NEW = 1,
    CONNTRACK_UPDATE = 2,
    CONNTRACK_DESTROY = 4,
    CONNTRACK_ALL = CONNTRACK_NEW | CONNTRACK_UPDATE | CONNTRACK_DESTROY
} conntrack_event_type;

typedef void (* conntrack_event_cb)(conntrack_t* conntrack, conntrack_event_type type, conntrack_data_t* data, void* priv);
typedef void (* conntrack_dump_cb)(conntrack_t* conntrack, conntrack_data_t* data, void* priv);

/**
 * Create a new conntrack_t.
 * @param conntrack         Pointer to a pointer for a new conntrack_t struct.
 * @param group             Group of connections you want to listen to.
 *                          Possible values: NF_NETLINK_CONNTRACK_NEW, NF_NETLINK_CONNTRACK_UPDATE, NF_NETLINK_CONNTRACK_DESTROY
 *                          Use 0 if you are not interested in receiving events and only want to query conntrack.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int conntrack_new(conntrack_t** conntrack, int group);

/**
 * Delete a conntrack_t struct.
 * @param conntrack       Pointer to a pointer to a valid conntrack_t struct.
 */
void conntrack_delete(conntrack_t** conntrack);

/**
 * Maximize the receive buffer of the conntrack netlink socket.
 * This should be used when ENOBUF error is happening too often in your application.
 * @param conntrack         conntrack_t struct that should maximize its buffer.
 *
 * @return 0 if suucesful, -1 otherwise.
 */
int conntrack_maximize_receive_buffer(conntrack_t* conntrack);

/**
 * Receive conntrack information about a connection.
 * @param conntrack        Initialized conntrack_t struct that should be used.
 * @param data             Conntrack_data_t struct that should already contain src, dst, src_prt, dst_port l3protocol and l4protocol.
 *                         Other information will be filled in by this function.
 *
 * @return 0 if connection was successfully looked up, -1 otherwise.
 */
int conntrack_get_connection_info(conntrack_t* conntrack, conntrack_data_t* data);

/**
 * Destroys a connection from conntrack.
 * @param conntrack        Initialized conntrack_t struct that should be used.
 * @param data             Conntrack_data_t struct that should contain src, dst, src_prt, dst_port l3protocol and l4protocol of the connection.
 * */
void conntrack_destroy_connection(conntrack_t* conntrack, const conntrack_data_t* data);

/**
 * Dump all conntrack connections.
 * @param conntrack     Initialized conntrack_t struct that should be used.
 * @param family        IP family of connections that should be dumped.
 * @param cb            Callback that will be called on each dumped connection
 * @param priv          Private pointer to be passed to the callback function.
 *
 * @return 0 if connection was successfully looked up, -1 otherwise.
 */
int conntrack_dump(conntrack_t* conntrack, uint32_t family, conntrack_dump_cb cb, void* priv);

/**
 * Returns the file descriptor of the conntrack_t socket.
 * @param conntrack         conntrack_t struct to receive a fd from.
 *
 * @return The fd if succesful, -1 otherwise.
 */
int conntrack_get_fd(conntrack_t* conntrack);

/**
 * Tries to create a conntrack_t struct from a file descriptor.
 * @param conntrack Pointer to a pointer to a valid conntrack_t struct.
 * @param fd        The netlink file descriptor
 *
 * @return 0 if succesful, -1 otherwise.
 */
int conntrack_get(conntrack_t** conntrack, int fd);

/**
 * Receives conntrack information of the conntrack_t struct.
 * @param conntrack       Initialized conntrack_t struct to read data off.
 *
 * @return 0 if succesful, -1 otherwise.
 */
int conntrack_read(conntrack_t* conntrack);

/**
 * Add an event listener to a conntrack_t struct.
 * The callback will be called whenever an event occurs.
 * @param conntrack         Initialized conntrack_t struct that should be used.
 * @param type              Conntrack event types that should be evented.
 * @param cb                Callback function to be called when connections are received.
 * @param priv              private pointer to be passed to the callback function.
 *
 * @return 0 if cb was added succesfully, -1 otherwise.
 */
int conntrack_add_event_cb(conntrack_t* conntrack, conntrack_event_type type, conntrack_event_cb cb, void* priv);

#endif /* __CONNTRACK_H__ */
