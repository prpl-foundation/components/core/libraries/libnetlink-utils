/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "netlink-utils/nfqueue.h"
#include "conntrack_priv.h"

#include <amxc/amxc_macros.h>

#include <errno.h>
#include <string.h>
#include <libmnl/libmnl.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nfnetlink.h>

#include <linux/netfilter/nfnetlink_queue.h>
#include <linux/netfilter/nfnetlink_conntrack.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "nfqueue"

struct nfqueue_t {
    uint16_t qnum;
    struct mnl_socket* nl;
};

typedef struct {
    nfqueue_t* nfqueue;
    nfqueue_data_t* nfqdata;
    packet_data_t* packet;
} nfqueue_data_priv_t;

int nfqueue_judge_packet(nfqueue_t* nfqueue, uint32_t pkt_id, uint32_t pkt_verdict) {
    struct nlmsghdr* nlh = NULL;
    struct nfgenmsg* nfg = NULL;
    struct nfqnl_msg_verdict_hdr verdict;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int ret = -1;

    when_null(nfqueue, exit);

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_QUEUE << 8) | NFQNL_MSG_VERDICT;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = AF_UNSPEC;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = htons(nfqueue->qnum);

    verdict.verdict = pkt_verdict;
    verdict.id = pkt_id;
    mnl_attr_put(nlh, NFQA_VERDICT_HDR, sizeof(verdict), &verdict);

    when_true_trace(mnl_socket_sendto(nfqueue->nl, nlh, nlh->nlmsg_len) < 0, exit, ERROR, "Could not send verdict");

    ret = 0;

exit:
    return ret;
}

int nfqueue_judge_packet_mark(nfqueue_t* nfqueue, uint32_t pkt_id, uint32_t pkt_verdict, uint32_t pkt_mark, uint32_t pkt_connmark) {
    struct nlmsghdr* nlh = NULL;
    struct nfgenmsg* nfg = NULL;
    struct nfqnl_msg_verdict_hdr verdict;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int ret = -1;

    when_null(nfqueue, exit);

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_QUEUE << 8) | NFQNL_MSG_VERDICT;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = AF_UNSPEC;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = htons(nfqueue->qnum);

    verdict.verdict = htonl(pkt_verdict);
    verdict.id = htonl(pkt_id);
    mnl_attr_put(nlh, NFQA_VERDICT_HDR, sizeof(verdict), &verdict);
    mnl_attr_put_u32(nlh, NFQA_MARK, htonl(pkt_mark));

#ifdef NFQA_CFG_F_CONNTRACK
    struct nlattr* nest = mnl_attr_nest_start(nlh, NFQA_CT);
    mnl_attr_put_u32(nlh, CTA_MARK, htonl(pkt_connmark));
    mnl_attr_nest_end(nlh, nest);
#else
    (void) pkt_connmark;
#endif

    when_true_trace(mnl_socket_sendto(nfqueue->nl, nlh, nlh->nlmsg_len) < 0, exit, ERROR, "Could not send verdict");

    ret = 0;

exit:
    return ret;
}

#ifdef NFQA_CFG_F_CONNTRACK
static int nfqueue_process_packet_conntrack(struct nlattr** attr, packet_data_t* packet) {
    int ret = -1;

    ret = conntrack_payload_parse(mnl_attr_get_payload(attr[NFQA_CT]), mnl_attr_get_payload_len(attr[NFQA_CT]),
                                  packet->family, &packet->conntrack_data);

    when_failed(ret, exit);

    packet->conntrack_info = ntohl(mnl_attr_get_u32(attr[NFQA_CT_INFO]));

exit:
    return ret;
}
#endif

static int nfqueue_attr_cb(struct nlattr const* attr, void* data) {
    struct nlattr const** tb = data;
    int type = mnl_attr_get_type(attr);

    when_true_trace((mnl_attr_type_valid(attr, NFQA_MAX) < 0), exit, ERROR, "Not a valid attr type [%d]", type);

    tb[type] = attr;

exit:
    return MNL_CB_OK;
}

static int nfqueue_process_packet(const struct nlmsghdr* nlh, void* data) {
    struct nfqnl_msg_packet_hdr* header = NULL;
    struct nlattr* attr[NFQA_MAX + 1] = {};
    struct nfgenmsg* nfmsg = NULL;
    nfqueue_data_priv_t* nfqueue_priv = (nfqueue_data_priv_t*) data;
    struct nfqnl_msg_packet_timestamp* ts = NULL;
    struct nfqnl_msg_packet_hw* hw = NULL;
    int ret = MNL_CB_ERROR;

    when_false_trace(mnl_attr_parse(nlh, sizeof(struct nfgenmsg), nfqueue_attr_cb, attr) == MNL_CB_OK, exit, ERROR, "Problem with parsing message");

    nfmsg = mnl_nlmsg_get_payload(nlh);

    when_null_trace(attr[NFQA_PACKET_HDR], exit, ERROR, "Metaheader was not set");
    header = mnl_attr_get_payload(attr[NFQA_PACKET_HDR]);

    nfqueue_priv->nfqdata->group = nfqueue_priv->nfqueue->qnum;
    nfqueue_priv->nfqdata->id = ntohl(header->packet_id);
    nfqueue_priv->packet->family = nfmsg->nfgen_family;
    nfqueue_priv->packet->hwproto = ntohs(header->hw_protocol);
    nfqueue_priv->packet->hook = header->hook;

    if(attr[NFQA_MARK]) {
        nfqueue_priv->packet->mark = ntohl(mnl_attr_get_u32(attr[NFQA_MARK]));
    }

    if(attr[NFQA_TIMESTAMP]) {
        ts = mnl_attr_get_payload(attr[NFQA_TIMESTAMP]);
        nfqueue_priv->packet->timestamp.tv_sec = ts->sec;
        nfqueue_priv->packet->timestamp.tv_usec = ts->usec;
    }
    if(attr[NFQA_IFINDEX_INDEV] && attr[NFQA_IFINDEX_OUTDEV]) {
        nfqueue_priv->packet->indev = ntohl(mnl_attr_get_u32(attr[NFQA_IFINDEX_INDEV]));
        nfqueue_priv->packet->outdev = ntohl(mnl_attr_get_u32(attr[NFQA_IFINDEX_OUTDEV]));
    }
    if(attr[NFQA_IFINDEX_PHYSINDEV] && attr[NFQA_IFINDEX_PHYSOUTDEV]) {
        nfqueue_priv->packet->physindev = ntohl(mnl_attr_get_u32(attr[NFQA_IFINDEX_PHYSINDEV]));
        nfqueue_priv->packet->physoutdev = ntohl(mnl_attr_get_u32(attr[NFQA_IFINDEX_PHYSOUTDEV]));
    }
    if(attr[NFQA_HWADDR]) {
        hw = mnl_attr_get_payload(attr[NFQA_HWADDR]);
        nfqueue_priv->packet->hwaddr_len = htons(hw->hw_addrlen);
        memcpy(nfqueue_priv->packet->hwaddr, hw->hw_addr, sizeof(uint8_t) * 8);
    }

    nfqueue_priv->packet->payload_len = mnl_attr_get_payload_len(attr[NFQA_PAYLOAD]);
    nfqueue_priv->packet->payload = calloc(1, mnl_attr_get_payload_len(attr[NFQA_PAYLOAD]));
    memcpy(nfqueue_priv->packet->payload, mnl_attr_get_payload(attr[NFQA_PAYLOAD]), mnl_attr_get_payload_len(attr[NFQA_PAYLOAD]));

#ifdef NFQA_CFG_F_CONNTRACK
    if(attr[NFQA_CT] && attr[NFQA_CT_INFO]) {
        nfqueue_process_packet_conntrack(attr, nfqueue_priv->packet);
    }
#endif

    ret = MNL_CB_OK;

exit:
    return ret;
}

int nfqueue_read(nfqueue_t* nfqueue, nfqueue_data_t* nfqdata, packet_data_t** packet) {
    int ret = -1;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    nfqueue_data_priv_t nfqueue_priv;

    when_null(nfqueue, exit);
    when_null(nfqdata, exit);
    when_null(packet, exit);

    when_failed(packet_data_new(packet), exit);
    memset(nfqdata, 0, sizeof(nfqueue_data_t));

    nfqueue_priv.nfqueue = nfqueue;
    nfqueue_priv.nfqdata = nfqdata;
    nfqueue_priv.packet = *packet;

    ret = mnl_socket_recvfrom(nfqueue->nl, buf, sizeof(buf));
    when_true_trace(ret < 0, exit, ERROR, "recv[%d]: %s", errno, strerror(errno));

    ret = mnl_cb_run(buf, ret, 0, mnl_socket_get_portid(nfqueue->nl), nfqueue_process_packet, &nfqueue_priv);
    when_true_trace(ret < 0, exit, ERROR, "Could not run message callback");

    if(ret == MNL_CB_OK) {
        ret = 0;
    }

exit:
    return ret;
}

int nfqueue_get_fd(nfqueue_t* nfqueue) {
    int fd = -1;

    when_null(nfqueue, exit);

    fd = mnl_socket_get_fd(nfqueue->nl);

exit:
    return fd;
}

uint16_t nfqueue_get_qnum(nfqueue_t* nfqueue) {
    uint16_t qnum = -1;

    when_null(nfqueue, exit);

    qnum = nfqueue->qnum;

exit:
    return qnum;
}

int nfqueue_get(nfqueue_t** nfqueue, int nlfd, int qnum) {
    int ret = -1;

    when_false(nlfd >= 0, exit);

    *nfqueue = calloc(1, sizeof(nfqueue_t));
    when_null(*nfqueue, exit);

    (*nfqueue)->qnum = qnum;
    (*nfqueue)->nl = mnl_socket_fdopen(nlfd);
    if(!(*nfqueue)->nl) {
        free(*nfqueue);
        *nfqueue = NULL;
        goto exit;
    }

    ret = 0;

exit:
    return ret;
}

static int nfqueue_config_bind_ip(nfqueue_t* nfqueue, int8_t ip_version, uint8_t command) {
    char buf[MNL_SOCKET_BUFFER_SIZE];
    struct nlmsghdr* nlh = NULL;
    struct nfgenmsg* nfg = NULL;
    struct nfqnl_msg_config_cmd nfq_command;
    int ret = -1;

    memset(buf, 0, sizeof(buf));

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_QUEUE << 8) | NFQNL_MSG_CONFIG;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = AF_UNSPEC;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = htons(nfqueue->qnum);

    if((ip_version == 0) || (ip_version == 4)) {
        nfq_command.command = command;
        nfq_command._pad = 0;
        nfq_command.pf = htons(AF_INET);
        mnl_attr_put(nlh, NFQA_CFG_CMD, sizeof(nfq_command), &nfq_command);
    }

    if((ip_version == 0) || (ip_version == 6)) {
        nfq_command.command = command;
        nfq_command.pf = htons(AF_INET6);
        mnl_attr_put(nlh, NFQA_CFG_CMD, sizeof(nfq_command), &nfq_command);
    }

    when_true_trace(mnl_socket_sendto(nfqueue->nl, nlh, nlh->nlmsg_len) < 0, exit, ERROR, "Could not %s mnl socket", command == NFQNL_CFG_CMD_UNBIND ? "unbind" : "bind");

    ret = 0;

exit:
    return ret;
}

static int nfqueue_config_ip_family(nfqueue_t* nfqueue, int8_t ip_version) {
    int ret = -1;

    when_failed(nfqueue_config_bind_ip(nfqueue, 0, NFQNL_CFG_CMD_UNBIND), exit);
    when_failed(nfqueue_config_bind_ip(nfqueue, ip_version, NFQNL_CFG_CMD_BIND), exit);

    ret = 0;

exit:
    return ret;
}

static int nfqueue_config(nfqueue_t* nfqueue, UNUSED bool conntrack_info) {
    struct nlmsghdr* nlh = NULL;
    struct nfgenmsg* nfg;
    struct nfqnl_msg_config_params params;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int ret = -1;

    memset(buf, 0, sizeof(buf));

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_QUEUE << 8) | NFQNL_MSG_CONFIG;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = AF_UNSPEC;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = htons(nfqueue->qnum);

    params.copy_mode = NFQNL_COPY_PACKET;
    params.copy_range = htonl(0xffff);
    mnl_attr_put(nlh, NFQA_CFG_PARAMS, sizeof(params), &params);

#ifdef NFQA_CFG_F_FAIL_OPEN
    int flags = NFQA_CFG_F_GSO | NFQA_CFG_F_FAIL_OPEN;

#ifdef NFQA_CFG_F_CONNTRACK
    /* NOTE: NFQA_CFG_F_CONNTRACK adds extra conntrack info to packet
     * Requires Linux kernel >= 3.6
     */
    if(conntrack_info) {
        flags |= NFQA_CFG_F_CONNTRACK;
    }
#else
    (void) conntrack_info;
#endif

    mnl_attr_put_u32(nlh, NFQA_CFG_FLAGS, htonl(flags));
    mnl_attr_put_u32(nlh, NFQA_CFG_MASK, htonl(flags));
#endif

    when_true_trace(mnl_socket_sendto(nfqueue->nl, nlh, nlh->nlmsg_len) < 0, exit, ERROR, "Could not configure queue");

    ret = 0;

exit:
    return ret;
}

static int nfqueue_connect(nfqueue_t* nfqueue, int8_t ip_version, UNUSED bool conntrack_info) {
    int option = 1;
    int ret = -1;

    when_null(nfqueue, exit);

    nfqueue->nl = mnl_socket_open(NETLINK_NETFILTER);
    when_null_trace(nfqueue->nl, exit, ERROR, "Could not create mnl socket")
    when_failed_trace(mnl_socket_bind(nfqueue->nl, 0, MNL_SOCKET_AUTOPID), exit, ERROR, "Could not bind mnl socket");

    when_failed(nfqueue_config_ip_family(nfqueue, ip_version), exit);
    when_failed(nfqueue_config(nfqueue, conntrack_info), exit);

    /* ENOBUFS is signalled to userspace when packets were lost
     * on kernel side.  In most cases, userspace isn't interested
     * in this information, so turn it off.
     */
    when_failed_trace(mnl_socket_setsockopt(nfqueue->nl, NETLINK_NO_ENOBUFS, &option, sizeof(int)), exit, ERROR, "Could not set sockopt");

    ret = 0;

exit:
    return ret;
}

int nfqueue_new(nfqueue_t** nfqueue, uint16_t qnum, int8_t ip_version, bool conntrack_info) {
    int ret = -1;

    *nfqueue = calloc(1, sizeof(nfqueue_t));
    when_null(*nfqueue, exit);

    (*nfqueue)->qnum = qnum;

    ret = nfqueue_connect(*nfqueue, ip_version, conntrack_info);

exit:
    if(ret != 0) {
        nfqueue_delete(nfqueue, true);
    }
    return ret;
}

void nfqueue_delete(nfqueue_t** nfqueue, bool close_socket) {
    when_null(*nfqueue, exit);

    if((*nfqueue)->nl) {
        if(close_socket) {
            mnl_socket_close((*nfqueue)->nl);
        } else {
            free((*nfqueue)->nl);
        }
        (*nfqueue)->nl = NULL;
    }
    free(*nfqueue);
    *nfqueue = NULL;

exit:
    return;
}
