/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "netlink-utils/nflog.h"
#include "conntrack_priv.h"

#include <amxc/amxc_macros.h>

#include <errno.h>
#include <string.h>
#include <libmnl/libmnl.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nfnetlink.h>

#include <linux/netfilter/nfnetlink_log.h>
#include <linux/netfilter/nfnetlink_conntrack.h>
#include <fcntl.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "nflog"

struct nflog_t {
    uint16_t gnum;
    struct mnl_socket* nl;
};

typedef struct {
    nflog_t* nflog;
    nflog_data_t* nfldata;
    packet_data_t* packet;
} nflog_data_priv_t;

#ifdef NFULNL_CFG_F_CONNTRACK
static int nflog_process_packet_conntrack(struct nlattr** attr, packet_data_t* nflog_data) {
    int ret = -1;

    ret = conntrack_payload_parse(mnl_attr_get_payload(attr[NFULA_CT]), mnl_attr_get_payload_len(attr[NFULA_CT]),
                                  nflog_data->family, &nflog_data->conntrack_data);

    when_failed(ret, exit);

    nflog_data->conntrack_info = ntohl(mnl_attr_get_u32(attr[NFULA_CT_INFO]));

exit:
    return ret;
}
#endif

static int nflog_attr_cb(struct nlattr const* attr, void* data) {
    struct nlattr const** tb = data;
    int type = mnl_attr_get_type(attr);

    when_true_trace((mnl_attr_type_valid(attr, NFULA_MAX) < 0), exit, ERROR, "Not a valid attr type [%d]", type);

    tb[type] = attr;

exit:
    return MNL_CB_OK;
}

static int nflog_process_packet(const struct nlmsghdr* nlh, void* data) {
    struct nfulnl_msg_packet_hdr* header = NULL;
    struct nlattr* attr[NFULA_MAX + 1] = {};
    struct nfgenmsg* nfmsg = NULL;
    nflog_data_priv_t* nflog_priv = (nflog_data_priv_t*) data;
    struct nfulnl_msg_packet_timestamp* ts = NULL;
    struct nfulnl_msg_packet_hw* hw = NULL;
    int ret = MNL_CB_ERROR;

    when_false_trace(mnl_attr_parse(nlh, sizeof(struct nfgenmsg), nflog_attr_cb, attr) == MNL_CB_OK, exit, ERROR, "Problem with parsing message");

    nfmsg = mnl_nlmsg_get_payload(nlh);

    when_null_trace(attr[NFULA_PACKET_HDR], exit, ERROR, "Metaheader was not set");
    header = mnl_attr_get_payload(attr[NFULA_PACKET_HDR]);

    nflog_priv->nfldata->group = nflog_priv->nflog->gnum;
    nflog_priv->packet->family = nfmsg->nfgen_family;
    nflog_priv->packet->hwproto = ntohs(header->hw_protocol);
    nflog_priv->packet->hook = header->hook;

    if(attr[NFULA_PREFIX]) {
        nflog_priv->nfldata->prefix = mnl_attr_get_payload(attr[NFULA_PAYLOAD]);
    }

    if(attr[NFULA_MARK]) {
        nflog_priv->packet->mark = ntohl(mnl_attr_get_u32(attr[NFULA_MARK]));
    }

    if(attr[NFULA_TIMESTAMP]) {
        ts = mnl_attr_get_payload(attr[NFULA_TIMESTAMP]);
        nflog_priv->packet->timestamp.tv_sec = ts->sec;
        nflog_priv->packet->timestamp.tv_usec = ts->usec;
    }
    if(attr[NFULA_IFINDEX_INDEV] && attr[NFULA_IFINDEX_OUTDEV]) {
        nflog_priv->packet->indev = ntohl(mnl_attr_get_u32(attr[NFULA_IFINDEX_INDEV]));
        nflog_priv->packet->outdev = ntohl(mnl_attr_get_u32(attr[NFULA_IFINDEX_OUTDEV]));
    }
    if(attr[NFULA_IFINDEX_PHYSINDEV] && attr[NFULA_IFINDEX_PHYSOUTDEV]) {
        nflog_priv->packet->physindev = ntohl(mnl_attr_get_u32(attr[NFULA_IFINDEX_PHYSINDEV]));
        nflog_priv->packet->physoutdev = ntohl(mnl_attr_get_u32(attr[NFULA_IFINDEX_PHYSOUTDEV]));
    }
    if(attr[NFULA_HWADDR]) {
        hw = mnl_attr_get_payload(attr[NFULA_HWADDR]);
        nflog_priv->packet->hwaddr_len = htons(hw->hw_addrlen);
        memcpy(nflog_priv->packet->hwaddr, hw->hw_addr, sizeof(uint8_t) * 8);
    }
    nflog_priv->packet->payload_len = mnl_attr_get_payload_len(attr[NFULA_PAYLOAD]);
    nflog_priv->packet->payload = calloc(1, mnl_attr_get_payload_len(attr[NFULA_PAYLOAD]));
    memcpy(nflog_priv->packet->payload, mnl_attr_get_payload(attr[NFULA_PAYLOAD]), mnl_attr_get_payload_len(attr[NFULA_PAYLOAD]));

#ifdef NFULNL_CFG_F_CONNTRACK
    if(attr[NFULA_CT] && attr[NFULA_CT_INFO]) {
        nflog_process_packet_conntrack(attr, nflog_priv->packet);
    }
#endif

    ret = MNL_CB_OK;

exit:
    return ret;
}

int nflog_read(nflog_t* nflog, nflog_data_t* nfldata, packet_data_t** packet) {
    int ret = -1;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    nflog_data_priv_t nflog_priv;

    when_null(nflog, exit);
    when_null(nfldata, exit);
    when_null(packet, exit);

    when_failed(packet_data_new(packet), exit);
    memset(nfldata, 0, sizeof(nflog_data_t));

    nflog_priv.nflog = nflog;
    nflog_priv.nfldata = nfldata;
    nflog_priv.packet = *packet;

    ret = mnl_socket_recvfrom(nflog->nl, buf, sizeof(buf));
    when_true_trace(ret < 0, exit, ERROR, "recv[%d]: %s", errno, strerror(errno));

    ret = mnl_cb_run(buf, ret, 0, mnl_socket_get_portid(nflog->nl), nflog_process_packet, &nflog_priv);
    when_true_trace(ret < 0, exit, ERROR, "Could not run message callback");

    if(ret == MNL_CB_OK) {
        ret = 0;
    }

exit:
    return ret;
}

int nflog_get_fd(nflog_t* nflog) {
    int fd = -1;

    when_null(nflog, exit);

    fd = mnl_socket_get_fd(nflog->nl);

exit:
    return fd;
}

uint16_t nflog_get_gnum(nflog_t* nflog) {
    uint16_t gnum = -1;

    when_null(nflog, exit);

    gnum = nflog->gnum;

exit:
    return gnum;
}

int nflog_get(nflog_t** nflog, int nlfd, int gnum) {
    int ret = -1;

    when_false(nlfd >= 0, exit);

    *nflog = calloc(1, sizeof(nflog_t));
    when_null(*nflog, exit);

    (*nflog)->gnum = gnum;
    (*nflog)->nl = mnl_socket_fdopen(nlfd);
    if(!(*nflog)->nl) {
        free(*nflog);
        *nflog = NULL;
        goto exit;
    }

    ret = 0;

exit:
    return ret;
}

static int nflog_config_bind_ip(nflog_t* nflog, uint8_t family, uint8_t command) {
    char buf[MNL_SOCKET_BUFFER_SIZE];
    struct nlmsghdr* nlh = NULL;
    struct nfgenmsg* nfg = NULL;
    struct nfulnl_msg_config_cmd nfl_command;
    int ret = -1;

    memset(buf, 0, sizeof(buf));

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_QUEUE << 8) | NFULNL_MSG_CONFIG;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = family;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = 0;

    nfl_command.command = command;
    mnl_attr_put(nlh, NFULA_CFG_CMD, sizeof(nfl_command), &nfl_command);

    when_true_trace(mnl_socket_sendto(nflog->nl, nlh, nlh->nlmsg_len) < 0, exit, ERROR, "Could not %s %s",
                    command == NFULNL_CFG_CMD_PF_UNBIND ? "unbind" : "bind", family == AF_INET ? "IPV4" : "IPV6");

    ret = 0;

exit:
    return ret;
}

static int nflog_config_ip_family(nflog_t* nflog, int8_t ip_version) {
    int ret = -1;

    when_failed(nflog_config_bind_ip(nflog, AF_INET, NFULNL_CFG_CMD_PF_UNBIND), exit);
    when_failed(nflog_config_bind_ip(nflog, AF_INET6, NFULNL_CFG_CMD_PF_UNBIND), exit);

    if((ip_version == 0) || (ip_version == 4)) {
        when_failed(nflog_config_bind_ip(nflog, AF_INET, NFULNL_CFG_CMD_BIND), exit);
    }

    if((ip_version == 0) || (ip_version == 6)) {
        when_failed(nflog_config_bind_ip(nflog, AF_INET6, NFULNL_CFG_CMD_BIND), exit);
    }

    ret = 0;

exit:
    return ret;
}

static int nflog_config(nflog_t* nflog, bool conntrack_info) {
    struct nlmsghdr* nlh = NULL;
    struct nfgenmsg* nfg = NULL;
    struct nfulnl_msg_config_mode mode;
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int ret = -1;

    memset(buf, 0, sizeof(buf));

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_ULOG << 8) | NFULNL_MSG_CONFIG;
    nlh->nlmsg_flags = NLM_F_REQUEST;

    nfg = mnl_nlmsg_put_extra_header(nlh, sizeof(*nfg));
    nfg->nfgen_family = AF_UNSPEC;
    nfg->version = NFNETLINK_V0;
    nfg->res_id = htons(nflog->gnum);

    mode.copy_mode = NFULNL_COPY_PACKET;
    mode.copy_range = htonl(0xffff);
    mode._pad = 0;

    mnl_attr_put(nlh, NFULA_CFG_MODE, sizeof(mode), &mode);
    mnl_attr_put_u32(nlh, NFULA_CFG_NLBUFSIZ, htonl(150000));
    mnl_attr_put_u32(nlh, NFULA_CFG_TIMEOUT, htonl(100));
    mnl_attr_put_u32(nlh, NFULA_CFG_QTHRESH, htonl(10));

#ifdef NFULNL_CFG_F_CONNTRACK
    /* NOTE: NFQA_CFG_F_CONNTRACK adds extra conntrack info to packet
     * Requires Linux kernel >= 3.6
     */
    if(conntrack_info) {
        mnl_attr_put_u16(nlh, NFULA_CFG_FLAGS, htons(NFULNL_CFG_F_CONNTRACK));
    }
#else
    (void) conntrack_info;
#endif

    when_true_trace(mnl_socket_sendto(nflog->nl, nlh, nlh->nlmsg_len) < 0, exit, ERROR, "Could not configure nflog");

    ret = 0;

exit:
    return ret;
}

static int nflog_connect(nflog_t* nflog, int8_t ip_version, bool conntrack_info) {
    int option = 1;
    int ret = -1;

    when_null(nflog, exit);

    nflog->nl = mnl_socket_open(NETLINK_NETFILTER);
    when_null_trace(nflog->nl, exit, ERROR, "Could not create mnl socket")
    when_failed_trace(mnl_socket_bind(nflog->nl, 0, MNL_SOCKET_AUTOPID), exit, ERROR, "Could not bind mnl socket");

    when_failed(nflog_config_ip_family(nflog, ip_version), exit);
    when_failed(nflog_config(nflog, conntrack_info), exit);

    /* ENOBUFS is signalled to userspace when packets were lost
     * on kernel side.  In most cases, userspace isn't interested
     * in this information, so turn it off.
     */
    when_failed_trace(mnl_socket_setsockopt(nflog->nl, NETLINK_NO_ENOBUFS, &option, sizeof(int)), exit, ERROR, "Could not set sockopt");

    ret = 0;

exit:
    return ret;
}

int nflog_new(nflog_t** nflog, uint16_t gnum, int8_t ip_version, bool conntrack_info) {
    int ret = -1;

    *nflog = calloc(1, sizeof(nflog_t));
    when_null(*nflog, exit);

    (*nflog)->gnum = gnum;

    ret = nflog_connect(*nflog, ip_version, conntrack_info);

exit:
    if(ret != 0) {
        nflog_delete(nflog, true);
    }

    return ret;
}

void nflog_delete(nflog_t** nflog, bool close_socket) {
    when_null(*nflog, exit);

    if((*nflog)->nl) {
        if(close_socket) {
            mnl_socket_close((*nflog)->nl);
        } else {
            free((*nflog)->nl);
        }
        (*nflog)->nl = NULL;
    }
    free(*nflog);
    *nflog = NULL;

exit:
    return;
}
