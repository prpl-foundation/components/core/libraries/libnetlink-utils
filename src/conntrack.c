/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "netlink-utils/conntrack.h"
#include "conntrack_priv.h"

#include <amxc/amxc_macros.h>

#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <time.h>
#include <endian.h>

#include <libmnl/libmnl.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nfnetlink.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define CT_SOCKET_BUFFER_SIZE         ((unsigned int) getpagesize() < 8192L ? (unsigned int) getpagesize() : 8192L)
#define CT_SOCKET_BUFFER_MAX_SIZE     ((unsigned int) getpagesize() * 128)

#ifndef SO_RCVBUFFORCE
#define SO_RCVBUFFORCE  (33)
#endif

#define ME "conntrack"

struct conntrack_priv_data_t {
    conntrack_data_t* data;
    int success;
};

struct conntrack_priv_dump_data_t {
    conntrack_t* conntrack;
    conntrack_dump_cb cb;
    void* priv;
    int success;
};

struct conntrack_attr_data_t {
    struct nlattr const** attr;
    uint32_t attr_max;
};

static int conntrack_attr_cb(struct nlattr const* attr, void* data) {
    struct conntrack_attr_data_t* attr_data = data;
    int type = mnl_attr_get_type(attr);
    int ret = MNL_CB_OK;

    when_true_trace(mnl_attr_type_valid(attr, attr_data->attr_max) < 0, exit, ERROR, "Not a valid attr type [%d]", type);

    attr_data->attr[type] = attr;

exit:
    return ret;
}

static int conntrack_parse_counters(conntrack_data_t* data, const struct nlattr* attr, bool original) {
    struct conntrack_attr_data_t attr_data;
    struct nlattr const* counters[CTA_COUNTERS_MAX + 1] = {};
    int ret = -1;

    attr_data.attr = counters;
    attr_data.attr_max = CTA_COUNTERS_MAX;
    when_true_trace(mnl_attr_parse_nested(attr, conntrack_attr_cb, &attr_data) < 0, exit, ERROR, "Could not parse counters");

    if(counters[CTA_COUNTERS_PACKETS]) {
        if(original) {
            data->packets_orig = be64toh(mnl_attr_get_u64(counters[CTA_COUNTERS_PACKETS]));
        } else {
            data->packets_repl = be64toh(mnl_attr_get_u64(counters[CTA_COUNTERS_PACKETS]));
        }
    }

    if(counters[CTA_COUNTERS_BYTES]) {
        if(original) {
            data->bytes_orig = be64toh(mnl_attr_get_u64(counters[CTA_COUNTERS_BYTES]));
        } else {
            data->bytes_repl = be64toh(mnl_attr_get_u64(counters[CTA_COUNTERS_BYTES]));
        }
    }

    ret = 0;

exit:
    return ret;
}

static int conntrack_parse_protocolinfo(conntrack_data_t* data, const struct nlattr* attr) {
    struct conntrack_attr_data_t attr_data;
    struct nlattr const* protoinfo[CTA_PROTOINFO_MAX + 1] = {};
    struct nlattr const* tcp_protoinfo[CTA_PROTOINFO_TCP_MAX + 1] = {};
    int ret = -1;

    attr_data.attr = protoinfo;
    attr_data.attr_max = CTA_PROTOINFO_MAX;
    when_true_trace(mnl_attr_parse_nested(attr, conntrack_attr_cb, &attr_data) < 0, exit, ERROR, "Could not parse protoinfo");

    if(protoinfo[CTA_PROTOINFO_TCP]) {
        attr_data.attr = tcp_protoinfo;
        attr_data.attr_max = CTA_PROTOINFO_TCP_MAX;
        when_true_trace(mnl_attr_parse_nested(protoinfo[CTA_PROTOINFO_TCP], conntrack_attr_cb, &attr_data) < 0, exit, ERROR, "Could not parse tcp protoinfo");
        if(tcp_protoinfo[CTA_PROTOINFO_TCP_STATE]) {
            data->tcp_state = mnl_attr_get_u8(tcp_protoinfo[CTA_PROTOINFO_TCP_STATE]);
        }
    }

    ret = 0;

exit:
    return ret;
}

static int conntrack_parse_tuple(conntrack_data_t* data, const struct nlattr* attr) {
    struct conntrack_attr_data_t attr_data;
    struct nlattr const* tuple[CTA_TUPLE_MAX + 1] = {};
    struct nlattr const* ip[CTA_IP_MAX + 1] = {};
    struct nlattr const* proto[CTA_PROTO_MAX + 1] = {};
    int ret = -1;

    attr_data.attr = tuple;
    attr_data.attr_max = CTA_TUPLE_MAX;
    when_true_trace(mnl_attr_parse_nested(attr, conntrack_attr_cb, &attr_data) < 0, exit, ERROR, "Could not parse tuple");

    if(tuple[CTA_TUPLE_IP]) {
        attr_data.attr = ip;
        attr_data.attr_max = CTA_IP_MAX;
        when_true_trace(mnl_attr_parse_nested(tuple[CTA_TUPLE_IP], conntrack_attr_cb, &attr_data) < 0, exit, ERROR, "Could not parse ip");
        if(ip[CTA_IP_V4_SRC] && ip[CTA_IP_V4_DST]) {
            data->l3protocol = AF_INET;
            data->src.addr.in4_addr.s_addr = mnl_attr_get_u32(ip[CTA_IP_V4_SRC]);
            data->dst.addr.in4_addr.s_addr = mnl_attr_get_u32(ip[CTA_IP_V4_DST]);
        }

        if(ip[CTA_IP_V4_SRC] && ip[CTA_IP_V6_DST]) {
            data->l3protocol = AF_INET6;

            memcpy(&data->src.addr.in6_addr, mnl_attr_get_payload(ip[CTA_IP_V6_SRC]), sizeof(struct in6_addr));
            memcpy(&data->dst.addr.in6_addr, mnl_attr_get_payload(ip[CTA_IP_V6_DST]), sizeof(struct in6_addr));
        }
    }

    if(tuple[CTA_TUPLE_PROTO]) {
        attr_data.attr = proto;
        attr_data.attr_max = CTA_PROTO_MAX;
        when_true_trace(mnl_attr_parse_nested(tuple[CTA_TUPLE_PROTO], conntrack_attr_cb, &attr_data) < 0, exit, ERROR, "Could not parse protocol");

        if(proto[CTA_PROTO_NUM]) {
            data->l4protocol = mnl_attr_get_u8(proto[CTA_PROTO_NUM]);
        }

        if(proto[CTA_PROTO_SRC_PORT] && proto[CTA_PROTO_DST_PORT]) {
            data->src_port = ntohs(mnl_attr_get_u16(proto[CTA_PROTO_SRC_PORT]));
            data->dst_port = ntohs(mnl_attr_get_u16(proto[CTA_PROTO_DST_PORT]));
        }
    }

    ret = 0;

exit:
    return ret;
}

int conntrack_payload_parse(const void* conntrack_payload, size_t len, UNUSED uint16_t l3num, conntrack_data_t* data) {
    struct conntrack_attr_data_t attr_data;
    struct nlattr const* attr[CTA_MAX + 1] = {};
    int ret = -1;

    when_null(data, exit);

    attr_data.attr = attr;
    attr_data.attr_max = CTA_MAX;
    when_true_trace(mnl_attr_parse_payload(conntrack_payload, len, conntrack_attr_cb, &attr_data) < 0, exit, ERROR, "Could not parse conntrack payload");

    if(attr[CTA_ID]) {
        data->id = ntohl(mnl_attr_get_u32(attr[CTA_ID]));
    }

    if(attr[CTA_MARK]) {
        data->mark = ntohl(mnl_attr_get_u32(attr[CTA_MARK]));
    }

    if(attr[CTA_TUPLE_ORIG]) {
        conntrack_parse_tuple(data, attr[CTA_TUPLE_ORIG]);
    }

    if(attr[CTA_COUNTERS_ORIG] && attr[CTA_COUNTERS_REPLY]) {
        conntrack_parse_counters(data, attr[CTA_COUNTERS_ORIG], true);
        conntrack_parse_counters(data, attr[CTA_COUNTERS_REPLY], false);
    }

    if(attr[CTA_PROTOINFO]) {
        conntrack_parse_protocolinfo(data, attr[CTA_PROTOINFO]);
    }

    ret = 0;

exit:
    return ret;
}

static int conntrack_build_message(struct nlmsghdr* nlh, const conntrack_data_t* data) {
    struct nlattr* tuple = NULL;
    struct nlattr* ip = NULL;
    struct nlattr* proto = NULL;
    int ret = -1;

    tuple = mnl_attr_nest_start(nlh, CTA_TUPLE_ORIG);
    when_null(tuple, exit);

    ip = mnl_attr_nest_start(nlh, CTA_TUPLE_IP);
    when_null(ip, exit);

    switch(data->l3protocol) {
    case AF_INET:
        mnl_attr_put_u32(nlh, CTA_IP_V4_SRC, data->src.addr.in4_addr.s_addr);
        mnl_attr_put_u32(nlh, CTA_IP_V4_DST, data->dst.addr.in4_addr.s_addr);
        break;
    case AF_INET6:
        mnl_attr_put(nlh, CTA_IP_V6_SRC, sizeof(struct in6_addr), &data->src.addr.in6_addr);
        mnl_attr_put(nlh, CTA_IP_V6_DST, sizeof(struct in6_addr), &data->dst.addr.in6_addr);
        break;
    default:
        goto exit;
    }

    mnl_attr_nest_end(nlh, ip);

    proto = mnl_attr_nest_start(nlh, CTA_TUPLE_PROTO);
    when_null(proto, exit);

    mnl_attr_put_u8(nlh, CTA_PROTO_NUM, data->l4protocol);
    mnl_attr_put_u16(nlh, CTA_PROTO_SRC_PORT, htons(data->src_port));
    mnl_attr_put_u16(nlh, CTA_PROTO_DST_PORT, htons(data->dst_port));

    mnl_attr_nest_end(nlh, proto);
    mnl_attr_nest_end(nlh, tuple);

    ret = 0;

exit:
    if(ret == -1) {
        mnl_attr_nest_cancel(nlh, proto);
        mnl_attr_nest_cancel(nlh, ip);
        mnl_attr_nest_cancel(nlh, tuple);
    }
    return ret;
}

static int conntrack_get_connection_cb(const struct nlmsghdr* nlh, void* priv) {
    struct nfgenmsg* nfhdr = mnl_nlmsg_get_payload(nlh);
    struct conntrack_priv_data_t* priv_data = (struct conntrack_priv_data_t*) priv;
    conntrack_data_t* data = NULL;

    when_null(priv_data, exit);

    data = priv_data->data;
    when_null(data, exit);

    when_false((nlh->nlmsg_type & 0XFF) == IPCTNL_MSG_CT_NEW, exit);

    when_failed(conntrack_payload_parse((uint8_t*) nfhdr + sizeof(struct nfgenmsg), mnl_nlmsg_get_payload_len(nlh) - sizeof(struct nfgenmsg),
                                        nfhdr->nfgen_family, data), exit);

    priv_data->success = true;

exit:
    return MNL_CB_OK;
}

static int conntrack_dump_connections_cb(const struct nlmsghdr* nlh, void* priv) {
    struct nfgenmsg* nfhdr = mnl_nlmsg_get_payload(nlh);
    struct conntrack_priv_dump_data_t* priv_data = (struct conntrack_priv_dump_data_t*) priv;
    conntrack_data_t data;
    bool ret = false;

    when_null(priv_data, exit);
    memset(&data, 0, sizeof(conntrack_data_t));

    when_false((nlh->nlmsg_type & 0XFF) == IPCTNL_MSG_CT_NEW, exit);

    when_failed(conntrack_payload_parse((uint8_t*) nfhdr + sizeof(struct nfgenmsg), mnl_nlmsg_get_payload_len(nlh) - sizeof(struct nfgenmsg),
                                        nfhdr->nfgen_family, &data), exit);
    priv_data->cb(priv_data->conntrack, &data, priv_data->priv);

    ret = MNL_CB_OK;

exit:
    if(ret != MNL_CB_OK) {
        priv_data->success = false;
    }
    return ret;
}

static int conntrack_event_connection_cb(const struct nlmsghdr* nlh, void* priv) {
    struct nfgenmsg* nfhdr = mnl_nlmsg_get_payload(nlh);
    conntrack_t* conntrack = (conntrack_t*) priv;
    conntrack_event_type type = CONNTRACK_NONE;
    conntrack_data_t data;

    when_null(conntrack, exit);
    memset(&data, 0, sizeof(conntrack_data_t));

    when_true((nlh->nlmsg_type >> 8) != NFNL_SUBSYS_CTNETLINK, exit);

    switch(nlh->nlmsg_type & 0xFF) {
    case IPCTNL_MSG_CT_NEW:
        if(nlh->nlmsg_flags & (NLM_F_CREATE | NLM_F_EXCL)) {
            type = CONNTRACK_NEW;
        } else {
            type = CONNTRACK_UPDATE;
        }
        break;
    case IPCTNL_MSG_CT_DELETE:
        type = CONNTRACK_DESTROY;
        break;
    default:
        SAH_TRACEZ_ERROR(ME, "Received unknown nlmsg type [%d]", nlh->nlmsg_type);
        goto exit;
    }
    when_true((type & conntrack->type_filter) == 0, exit);

    when_failed(conntrack_payload_parse((uint8_t*) nfhdr + sizeof(struct nfgenmsg), mnl_nlmsg_get_payload_len(nlh) - sizeof(struct nfgenmsg),
                                        nfhdr->nfgen_family, &data), exit);
    conntrack->cb(conntrack, type, &data, conntrack->priv);

exit:
    return MNL_CB_OK;
}

int conntrack_get_connection_info(conntrack_t* conntrack, conntrack_data_t* data) {
    struct nlmsghdr* nlh;
    struct nfgenmsg* nfh;
    struct conntrack_priv_data_t priv_data = {
        .data = NULL,
        .success = false
    };
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int seq = 0;
    int size = 0;
    int ret = -1;

    memset(buf, 0, sizeof(buf));

    when_null(conntrack, exit);
    when_null(data, exit);

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_CTNETLINK << 8) | IPCTNL_MSG_CT_GET;
    nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK;
    nlh->nlmsg_seq = seq = time(NULL);

    nfh = mnl_nlmsg_put_extra_header(nlh, sizeof(struct nfgenmsg));
    nfh->nfgen_family = data->l3protocol;
    nfh->version = NFNETLINK_V0;
    nfh->res_id = 0;

    priv_data.data = data;

    when_failed_trace(conntrack_build_message(nlh, data), exit, ERROR, "Could not create netlink message");
    when_true_trace(mnl_socket_sendto(conntrack->nl, nlh, nlh->nlmsg_len) < 0, exit, WARNING, "sendto[%d]: %s", errno, strerror(errno));

    when_false((size = mnl_socket_recvfrom(conntrack->nl, buf, sizeof(buf))) > 0, exit);
    when_true_trace(mnl_cb_run(buf, size, seq, mnl_socket_get_portid(conntrack->nl), conntrack_get_connection_cb, &priv_data) == MNL_CB_ERROR, exit,
                    INFO, "Could not run message callback");

    ret = priv_data.success ? 0 : -1;

exit:
    return ret;
}

void conntrack_destroy_connection(conntrack_t* conntrack, const conntrack_data_t* data) {
    struct nlmsghdr* nlh;
    struct nfgenmsg* nfh;
    char buf[MNL_SOCKET_BUFFER_SIZE];

    memset(buf, 0, sizeof(buf));

    when_null(conntrack, exit);
    when_null(data, exit);

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_CTNETLINK << 8) | IPCTNL_MSG_CT_DELETE;
    nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK;
    nlh->nlmsg_seq = time(NULL);

    nfh = mnl_nlmsg_put_extra_header(nlh, sizeof(struct nfgenmsg));
    nfh->nfgen_family = data->l3protocol;
    nfh->version = NFNETLINK_V0;
    nfh->res_id = 0;

    when_failed_trace(conntrack_build_message(nlh, data), exit, ERROR, "Could not create netlink message");
    when_true_trace(mnl_socket_sendto(conntrack->nl, nlh, nlh->nlmsg_len) < 0, exit, WARNING, "sendto[%d]: %s", errno, strerror(errno));

exit:
    return;
}

int conntrack_dump(conntrack_t* conntrack, uint32_t family, conntrack_dump_cb cb, void* priv) {
    struct nlmsghdr* nlh;
    struct nfgenmsg* nfh;
    struct conntrack_priv_dump_data_t priv_data = {
        .conntrack = conntrack,
        .cb = cb,
        .priv = priv,
        .success = true
    };
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int seq = 0;
    int size = 0;
    int ret = -1;

    when_null(conntrack, exit);

    nlh = mnl_nlmsg_put_header(buf);
    nlh->nlmsg_type = (NFNL_SUBSYS_CTNETLINK << 8) | IPCTNL_MSG_CT_GET;
    nlh->nlmsg_flags = NLM_F_REQUEST | NLM_F_DUMP;
    nlh->nlmsg_seq = time(NULL);
    seq = nlh->nlmsg_seq;

    nfh = mnl_nlmsg_put_extra_header(nlh, sizeof(struct nfgenmsg));
    nfh->nfgen_family = family;
    nfh->version = NFNETLINK_V0;
    nfh->res_id = 0;

    when_true_trace(mnl_socket_sendto(conntrack->nl, nlh, nlh->nlmsg_len) < 0, exit, WARNING, "sendto[%d]: %s", errno, strerror(errno));

    while((size = mnl_socket_recvfrom(conntrack->nl, buf, sizeof(buf))) > 0) {
        ret = mnl_cb_run(buf, size, seq, mnl_socket_get_portid(conntrack->nl), conntrack_dump_connections_cb, &priv_data);
        when_true_trace(ret == MNL_CB_ERROR, exit, WARNING, "Could not run message callback");
        if(ret == MNL_CB_STOP) {
            break;
        }
    }

    ret = priv_data.success ? 0 : -1;

exit:
    return ret;
}

int conntrack_add_event_cb(conntrack_t* conntrack, conntrack_event_type type, conntrack_event_cb cb, void* priv) {
    int ret = -1;

    when_null(conntrack, exit);

    conntrack->type_filter = type;
    conntrack->priv = priv;
    conntrack->cb = cb;

    ret = 0;

exit:
    return ret;
}

int conntrack_get_fd(conntrack_t* conntrack) {
    int fd = -1;

    when_null(conntrack, exit);

    fd = mnl_socket_get_fd(conntrack->nl);

exit:
    return fd;
}

static int conntrack_set_rcvbufsize(conntrack_t* conntrack, unsigned int rcvbufisze) {
    unsigned int read_size = rcvbufisze;
    socklen_t socklen = sizeof(read_size);
    int ret = -1;

    /* first we try the FORCE option, which is introduced in kernel
     * 2.6.14 to give "root" the ability to override the system wide
     * maximum */
    ret = setsockopt(mnl_socket_get_fd(conntrack->nl), SOL_SOCKET, SO_RCVBUFFORCE, &read_size, socklen);
    if(ret < 0) {
        /* if this didn't work, we try at least to get the system
         * wide maximum (or whatever the user requested) */
        setsockopt(mnl_socket_get_fd(conntrack->nl), SOL_SOCKET, SO_RCVBUF, &read_size, socklen);
    }
    getsockopt(mnl_socket_get_fd(conntrack->nl), SOL_SOCKET, SO_RCVBUF, &conntrack->rcv_buf_size, &socklen);

    ret = 0;

    return ret;
}

static int conntrack_increase_buf(conntrack_t* conntrack, unsigned int buf_size) {
    unsigned int old = 0;
    unsigned int new = 0;
    int ret = -1;

    when_null(conntrack, exit);

    if(conntrack->rcv_buf_size < CT_SOCKET_BUFFER_MAX_SIZE) {
        old = conntrack->rcv_buf_size;

        if(buf_size != 0) {
            new = buf_size;
        } else {
            new = old * 2;
        }

        if(new > CT_SOCKET_BUFFER_MAX_SIZE) {
            new = CT_SOCKET_BUFFER_MAX_SIZE;
        }

        new = new / 2; // size gets doubled somewhere within function call

        conntrack_set_rcvbufsize(conntrack, new);
        SAH_TRACEZ_INFO(ME, "Increased receive buffer size from [%u] to [%u] bytes", old, conntrack->rcv_buf_size);
        ret = 0;
    } else {
        SAH_TRACEZ_WARNING(ME, "Unable to increase receive buffer size, already at limit of [%u] bytes", CT_SOCKET_BUFFER_MAX_SIZE);
    }

exit:
    return ret;
}

int conntrack_maximize_receive_buffer(conntrack_t* conntrack) {
    return conntrack_increase_buf(conntrack, CT_SOCKET_BUFFER_MAX_SIZE);
}

int conntrack_read(conntrack_t* conntrack) {
    char buf[MNL_SOCKET_BUFFER_SIZE];
    int err = 0;
    int ret = 0;

    when_null(conntrack, exit);

    ret = mnl_socket_recvfrom(conntrack->nl, buf, sizeof(buf));
    if(ret < 0) {
        SAH_TRACEZ_WARNING(ME, "recv[%d]: %s", errno, strerror(errno));
        goto exit;
    }

    ret = mnl_cb_run(buf, ret, 0, 0, conntrack_event_connection_cb, conntrack);
    if(ret == -1) {
        err = errno;
        if((err != EAGAIN) && (err != EWOULDBLOCK)) {
            SAH_TRACEZ_ERROR(ME, "failed to read data, errno[%d]: %s", errno, strerror(errno));

            // Increase buffer size if buffer is full.
            // If this error happens too often, consider maximizing buf size from start of the application.
            if(err == ENOBUFS) {
                conntrack_increase_buf(conntrack, 0);
            }
        }
    }

exit:
    return ret;
}

int conntrack_get(conntrack_t** conntrack, int nlfd) {
    int ret = -1;

    when_null(conntrack, exit);
    when_false(nlfd >= 0, exit);

    *conntrack = calloc(1, sizeof(conntrack_t));
    when_null(*conntrack, exit);

    (*conntrack)->nl = mnl_socket_fdopen(nlfd);
    if(!(*conntrack)->nl) {
        free(*conntrack);
        *conntrack = NULL;
        goto exit;
    }

    conntrack_set_rcvbufsize(*conntrack, CT_SOCKET_BUFFER_SIZE);

    ret = 0;

exit:
    return ret;
}

static int conntrack_connect(conntrack_t* conntrack, int group) {
    int ret = -1;

    when_null(conntrack, exit);

    conntrack->nl = mnl_socket_open(NETLINK_NETFILTER);
    if(!conntrack->nl) {
        SAH_TRACEZ_ERROR(ME, "Could not create mnl socket");
        goto exit;
    }

    if(mnl_socket_bind(conntrack->nl, group, MNL_SOCKET_AUTOPID) < 0) {
        SAH_TRACEZ_ERROR(ME, "Could not bind mnl socket");
        mnl_socket_close(conntrack->nl);
        goto exit;
    }

    if(group != 0) {
        /*
         * Make socket non-blocking, this is mandatory to make everything work with the Parser.
         * Without a non-blocking socket, the application will block on it plus we'll have no
         * clear point when we can force it to stop. This would eliminate the read-loop.
         * With non-blocking, we'll receive an EAGAIN/EWOULDBLOCK error, knowing we have
         * to return to the application.
         */
        int flags = fcntl(mnl_socket_get_fd(conntrack->nl), F_GETFL, 0);
        if(flags < 0) {
            SAH_TRACEZ_ERROR(ME, "socket fnctl F_GETFL failed: [%m]");
            goto exit;
        }
        if(fcntl(mnl_socket_get_fd(conntrack->nl), F_SETFL, flags | O_NONBLOCK) < 0) {
            SAH_TRACEZ_ERROR(ME, "socket fnctl F_GETFL failed: [%m]");
            goto exit;
        }

        conntrack_set_rcvbufsize(conntrack, CT_SOCKET_BUFFER_SIZE);
    }

    ret = 0;

exit:
    return ret;
}

int conntrack_new(conntrack_t** conntrack, int group) {
    int ret = -1;

    when_null(conntrack, exit);

    *conntrack = calloc(1, sizeof(conntrack_t));
    when_null(*conntrack, exit);

    ret = conntrack_connect(*conntrack, group);

exit:
    if(ret != 0) {
        conntrack_delete(conntrack);
    }
    return ret;
}

void conntrack_delete(conntrack_t** conntrack) {
    when_null(conntrack, exit);
    when_null(*conntrack, exit);

    if((*conntrack)->nl) {
        mnl_socket_close((*conntrack)->nl);
        (*conntrack)->nl = NULL;
    }
    free(*conntrack);
    *conntrack = NULL;

exit:
    return;
}
