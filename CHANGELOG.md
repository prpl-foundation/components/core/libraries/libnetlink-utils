# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.3 - 2024-12-13(13:52:45 +0000)

### Other

- - libpacket-interception: libnetlink-utils: linking with GPL-2.0 licensed libnetfilter_* libraries

## Release v0.1.2 - 2024-06-21(10:47:40 +0000)

### Changes

- [ConnDB] Connections are not being deleted from ConnDB

## Release v0.1.1 - 2024-05-28(08:10:37 +0000)

### Changes

- Build error on undefined versions of sah_libnetlink-utils

## Release v0.1.0 - 2024-04-26(11:49:18 +0000)

### New

- [Conntrack] Create the new AMX Conntrack component

## Release v0.0.14 - 2024-04-08(14:26:50 +0000)

### Other

- [ServiceID][ConnDB] Conntrack callback not called

## Release v0.0.13 - 2024-03-05(16:08:11 +0000)

### Other

- Disable debian package generation

## Release v0.0.12 - 2024-03-05(14:58:35 +0000)

### Changes

- [PacketInterception] Add Flowmonitor support

## Release v0.0.11 - 2024-02-19(11:11:25 +0000)

### Changes

- [PacketInterception] Add NFLOG support

## Release v0.0.10 - 2024-01-04(14:18:27 +0000)

### Changes

- [ServiceID] Update Config options for SOP

## Release v0.0.9 - 2023-10-28(07:13:36 +0000)

### Changes

- [libnetlink-utils] netlink socket buffer not increasing

## Release v0.0.8 - 2023-10-09(08:42:01 +0000)

### Changes

- [ServiceIdentification] Increase performance

## Release v0.0.7 - 2023-08-17(09:29:09 +0000)

### Changes

- [ConnDB] Create the new AMX Connection Database component.

## Release v0.0.6 - 2023-05-04(07:04:10 +0000)

### Changes

- [ServiceID] Implement HTTP and HTPPs Service Identification

## Release v0.0.5 - 2022-12-12(11:57:19 +0000)

### Fixes

- Correct syntax for option to compile with libraries installed at /opt/prplos

## Release v0.0.4 - 2022-12-11(17:05:16 +0000)

### Fixes

- Add option to compile with libraries installed at /opt/prplos

## Release v0.0.3 - 2022-11-29(14:20:07 +0000)

### Other

- Opensource component

## Release v0.0.2 - 2022-11-29(12:28:53 +0000)

### Other

- [PacketInterception] WNC board crashes

## Release v0.0.1 - 2022-11-17(09:31:40 +0000)

### Other

- [Packet Interception] Create the new Packet Interception component

