# Netlink Utilities Library

[[_TOC_]]

## Introduction

libnetlink-utils is a library that provides an easy API for handling netlink sockets and sending/receiving information over the sockets.

The following netlink services are supported:
    
- NFQUEUE 
- NFLOG
- Conntrack

## Build instructions

Before building this library, its dependencies need to be installed:

```
sudo apt update && sudo apt install libamxc sah-lib-sahtrace-dev libmnl
```

Then the library can be built with

```
make && sudo -E make install  
```

## Netlink Utilities user guide

### NFQUEUE

#### Introduction

NFQUEUE is an iptables extension that allows packets to be intercepted and put into a queue. There are multiple queues available and they are represented by a specific queue number. The intercepted packets can be send to userspace via a netlink socket, where they can be analysed further. Every packet will have a unique ID for their queue.
A verdict must be returned over the netlink socket to let the queue know if the packet should be accepted, dropped, ...

#### API

A NFQUEUE netlink socket is represented by a `nfqueue_t` struct. The struct can be created by calling one of the following functions:

- `int nfqueue_new(nfqueue_t** nfqueue, uint16_t qnum, int8_t ip_version, bool conntrack_info);`
- `int nfqueue_get(nfqueue_t** nfqueue, int fd, int qnum);`

The struct should be deleted with:

- `void nfqueue_delete(nfqueue_t** nfqueue, bool close_socket);`

It is necessary to monitor the underlying file descriptor to see if any messages are available. The fd can be obtained with `nfqueue_get_fd`. The function `nfqueue_read` reads data from the socket and puts the packet information into a `nfqueue_data_t` struct and `packet_data_t` struct. The `packet_data_t` struct should be cleaned up using `packet_data_cleanup`.

Every intercepted packet should receive a verdict. It is possible to also alter the skbmark and connmark at this time. `nfqueue_judge_packet` simply sets a verdict on a packet. `nfqueue_judge_packet_mark` also alters the skbmark of the packet and connmark of the connection the packet belongs to.

### NFLOG

#### Introduction

NFLOG is an iptables extension that allows packets to be logged to userspace. There are several groups available where packets can be logged to and each of them has an unique group ID. The logged packets can be send to userspace via a netlink socket. Once the packet is send to userspace, it will continue traversing the following iptables rules and chains.

#### API

A NFLOG netlink socket is represented by a `nflog_t` struct. The struct can be created by calling one of the following functions:

- `int nflog_new(nflog_t** nflog, uint16_t gnum, int8_t ip_version, bool conntrack_info);`
- `int nflog_get(nflog_t** nflog, int fd, int qnum);`

The struct should be deleted with:

- `void nflog_delete(nflog_t** nflog, bool close_socket);`

It is necessary to monitor the underlying file descriptor to see if any messages are available. The fd can be obtained with `nflog_get_fd`. The function `nflog_read` reads data from the socket and puts the packet information into a `nflog_data_t` struct and `packet_data_t` struct. The `packet_data_t` struct should be cleaned up using `packet_data_cleanup`.

### Conntrack

#### Introduction

Conntrack is a kernel utility that monitors connections. It keeps track of information about connections that are still active. For instance how many bytes/packets were sent/received by a connection, the connmark, ... .
It is possible to interact with conntrack by using a netlink socket to communicate with it.

#### API

#### Conntrack_t struct

A Conntrack netlink socket is represented by a `conntrack_t` struct. The struct can be created by calling the following function:

- `int conntrack_new(conntrack_t** conntrack, int group);`

The struct should be deleted with:

- `void conntrack_delete(conntrack_t** conntrack);`

#### Conntrack queries

It is possible to receive conntrack information of a connection, using it's 5 tuple. The function `conntrack_get_connection_info` will fill in the `conntrack_data_t` struct if the fields corresponding to the 5 tuple (l3protocol, l4protocol, src, dst, src_port, dst_port) were already provided and the connection is being tracked by conntrack.

`conntrack_remove_connection` is used to remove connections from conntrack. A conntrack_data_t struct where the 5 tuple is filled in should be provided to select the connection to remove.

To retrieve all connections from conntrack, the function `conntrack_dump` can be used. A callback will be called for every connection retrieved.

#### Conntrack events

It is also possible to event connections being created, updated or destroyed in conntrack. The function `conntrack_add_event_cb` adds a callback to the conntrack_t struct and will be called whenever a connection is created, updated or destroyed. In this case, it is necessary to monitor the underlying file descriptor to see if any messages are available. The fd can be obtained with `conntrack_get_fd` and `conntrack_read` should be called when data is available.

Keep in mind that using conntrack queries and conntrack events on the same conntrack_t struct will result in unspecified behavior and should be avoided.

